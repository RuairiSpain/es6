 // Imports
 const htmlWebpackPlugin = require('html-webpack-plugin')
 require("babel-register");
 // Webpack Configuration
 const config = {
   // Entry
   entry: './src/app.js',
   // Output
   output: {
     path: __dirname + '/dist',
     filename: 'bundle.js'
   },
   devtool: 'source-map',
   // Loaders
   module: {
     rules : [
       // JavaScript/JSX Files
       {
       test: /\.js$/,
       exclude: /node_modules/,
       use: ['babel-loader']
       },
       // CSS Files
       {
       test: /\.css$/,
       use: ['style-loader', 'css-loader']
       }
     ]
   },
   // Plugins
   plugins: [
     new htmlWebpackPlugin({
       title: 'To Do ES6',
       filename: 'index.html',
       template: __dirname + '/src/index.html'
     })
   ],
   devServer: {
     proxy: {
        '/get': {
           target: "https://postman-echo.com",
           secure: false,
           pathRewrite: {
              '^/get': 'get'
           },
           logLevel: 'debug'
        }
     }
  },
 };
 // Exports
 module.exports = config;
 