# Part 1
1. In terminal, change directory to es6 project folder cd c:\ES6

2. Using git see the git branches: `git branch`

3. Check you are in the `master` branch.  If not checkout the master branch, with `git checkout master`

4. Start VSCode in the current folder, `code .`

5. Open the terminal windows inside VSCode, `Ctrl~`

6. Create a new file called `c:\ES6\sol11.html` 

7. Add the following HTML:
```HTML
    <html>
    <head>
        <meta name="viewport" content="width=500,initial-scale=1.0,user-scalable=yes">
        <meta charset="UTF-8">
        <title>Welcome to ES6</title>
    </head>
    <body>
        <div id="app"></div>
    </body> 
    </html>
```

8. Add a JavaScript script tag, we will add inline Javascript in this exercise and later refactor it in later exercises

9. Inside the script, add the following code:
- Review the code and make sure you are comfortable with each line.  If you have any questions ask your instructor.
```JS
    var dom = document.querySelector('#app');
    dom.innerHTML = "<input name='keywords'/><div id='main'></div>";

    document.querySelector("input").addEventListener("keyup", search);

    function search(event) {
        if (event.key === 'Enter') {
            var val = document.querySelector("input").value;
            //part 1
            document.querySelector("#main").innerHTML = "You searched for: " + val;

        }
        event.preventDefault();
    }
```
10. In File Explorer, open the `sol11.html` in Chrome.  Test the page is working by typing into the input box and pressing enter.  You should see the form input added to the page.  If it is not working open the Chrome DevTools, with `Command+Option+J`, (on Mac and Linux use `Control+Shift+J`).  Check for console errors and run the JavaScript Source debugger.

# Part 2
11. Modify the line after `//part 1` so it looks like:
- This calls a function, which we'll create in step 12.  Also, it adds a `double click event listener to remove the clicked item from the HTML document.
```JS
            //part 2
            var node = add("#main", val);
            node.addEventListener("dblclick", function(event) {
                event.target.outerHTML = "";
            });

```

12. Add a function, with the following code, it creates a DOM LI element with a DOM text node and appends it to the HTML document (placing it inside the `id` DOM node):
- This has 2 parameters, the first takes a CSS selector, the second is the text to add to a HTML LI list
```JS
    function add(id, value) {
        var node =  document.createElement("LI");
        var textnode = document.createTextNode(value);
        node.appendChild(textnode);
        document.querySelector(id).appendChild(node);
        return node;
    }
```

13. In File Explorer, open the `sol11.html` in Chrome.  Test the page is working by typing into the input box and pressing enter.  Add multiple list items to the page.  Test that if you `double click` a list item it is removed from the HTML page.

# Bonus
14.  Add some CSS styles to change the look of the LI list.  Remove the bullet, add a border, add a small amount of padding and margin to the LI.
