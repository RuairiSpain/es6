#Part 1

0. In git create the branch to `ex4.2`, with `git checkout -b ex4.2`. If you didn't finish the last exercise then use `git checkout sol4.1`

1. In VSCode, edit `./src/geo.js`
- Create a function called `getJson(url)`
- Inside the function, call `fetch(url)`, this returns a promise
- Chain the promise result into a `then()`, inside the resolved callback convert the results from a JSON string into an object with `.json()`
- Export `getJson` (not as default)

```JS
const getJson = (url) => fetch(url).then((response)  => response.json());
export { getJson };
```

2. Edit `./src/todo.js`, update the interpolated string inside the `render` call in `initialize()`
- In the HTML string add a new DIV with id `address`

Solution
```HTML
<div id='address'></div>
```

3. Edit `./src/app.js`
- Update the import for `geo.js`, so it looks like: `import where, { getJson } from './geo';`
- Add the following lines after the creation of the `mapboxgl` map:

Solution:
```JS
      const lookup = `https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat=${pos.coords.latitude}&lon=${pos.coords.longitude}`;
      getJson(lookup).then( result => {
        console.log(result.display_name);
        app.render('#address', result.display_name);
      });
```

5. Check the code runs correctly in the Browser, reminder to start the app run: `npm start` this builds the app and starts the Browser.
