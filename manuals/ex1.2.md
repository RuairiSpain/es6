#Part 1

0. In git change to the branch to `ex1.2`, with `git checkout ex1.2`

1. In VSCode, edit `package.json` and modify the scripts section so it looks like:
- This adds hot reloading and inline editing
```JS
"scripts": {
    "start": "webpack-dev-server --mode development --hot --inline",
    "build": "webpack",
    "dev": "webpack --mode development && webpack-dev-server --mode development --open-page dist"
  }
```

2. Install NPM dependencies, with `npm install`

3. Start the developer Web server with `npm start`, this should open Chrome with our index page.
- This command also adds a watch to look for any updates to the files in `./src`,
- if the source is updated then the babel loader is run and the page refreshed.  We have live reloading.

4. Review the folder layout of the Webpack project:
- `./src` contains the js and html files we'll edit
- `./dist` will contain the production ready files for deployment
- `package.jon` has the NPM dependencies and npm script aliases
- `webpack.config.js` has the Webpack configuration for our project
   - entry point is `./src/app.js`
   - output is in `./dist/bundle.js`
   - `devtool` say to add the sourcemap files to the developer bundles
   - `module` loaders include: babel loader for `.js` files and css-loader for `.css` files
   - We'll use `htmlWebpackPlugin` to use `./src/index.html` as the skeleton HTML for out `./dist/index.html` for production
   - Our `devServer` will proxy any request to `localhost:8080/get` and forward them to `https://postman-echo.com/get`, this allows use to make AJAX requests without needing CORS on our Web services

5. Cut the JavaScript code from `sol11.html` and move it to `app.js`
Solution in app.js:
```JS
var dom = document.querySelector('#app');
dom.innerHTML = "<input name='keywords'/><div id='main'></div>";

document.querySelector("input").addEventListener("keyup", search);

function search(event) {
  if (event.key === 'Enter') {
    var val = document.querySelector("input").value;
    //part 1
    // document.querySelector("#main").innerHTML = "You searched for: " + val;
    //part 2
    var node = add("#main", val);
    //part 3
    node.addEventListener("dblclick", function(event) {
      event.target.outerHTML = "";
    });
  }
	event.preventDefault();
}

function add(id, value) {
  var node =  document.createElement("LI");
  var textnode = document.createTextNode(value);
  node.appendChild(textnode);
  document.querySelector(id).appendChild(node);
  return node;
}

//Bonus
function httpEcho(request) {
  fetch( "/get?keywords=" + request).then(function(response) {
    return response.json();
  }).then(function(data) {
    document.querySelector("#main").innerHTML = "You searched for:" + data.args.keywords;
  });
}
```

6. Test the Browser and see that the Web app works correctly using Babel and Webpack.