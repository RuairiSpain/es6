#Setup
1. Install Visual Studio Code, from https://code.visualstudio.com/download
- Install Extensions `Settings Sync` and reload
- Type: `Ctrl-P`, then `>sync`, then select `Sync: Download Settings`, then
- then paste this Personal Access Token:  `03b05194a3c46ca220f13afeb2ee1b293426cbc1`
- then paste this Gist ID: `101b719b56cf1104b4173c6329e27886`
- Restart VSCode

2. Install NodeJS version 10+, from https://nodejs.org/en/download/

3. Install Chrome, from https://www.google.com/chrome
- Add extensions: Augury and Redux DevTools

4. Install git, from: https://git-scm.com/downloads

5. Visit https://gitlab.com/RuairiSpain/a6/blob/master/ES6.zip and click Download.  To download the ES6 course exercise files.  Then unzip to c:\.  When unzipped the files should be in C:\ES6\*

6. Visit https://gitlab.com/RuairiSpain/a6/blob/master/A6.zip and click Download.  To download the Angular course exercise files.  Then unzip to c:\.  When unzipped the files should be in C:\A6\*

7. Updated A6 Manual instructions, should be unzipped to `C:\A6\manuals`: https://drive.google.com/file/d/1lqU25JKVNj4rqDEWsRAMlJr7_HB4Uovk/view?usp=sharing

