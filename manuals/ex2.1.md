#Part 1

0. In git create the branch to `ex2.1`, with `git checkout -b ex2.1`. If you didn't finish the last exercise then use `git checkout sol1.2`

1. In VSCode, edit `./src/app.js` and update the code to ES6 syntax using the following steps:

2. Change the HTML string in `dom.innerHTML` to use an interpolated string.  Also, add a new HTML button with id `clear`, this will be used to clear the TO DO list

- Solution:
```JS
dom.innerHTML = `<input name='keywords'/>
                 <div id='main'></div>
                 <button id='clear'>Clear all</button>`;
```
3. Add a new DOM click listener for the `#clear` DOM node, using the function `document.querySelector("#clear")`.
- The addEvent listener should use an anonymous arrow function, and it's callback will change the DOM `#main` node to an empty string.
- Solution:
```JS
document.querySelector("#clear").addEventListener("click", () => {
  document.querySelector("#main").innerHTML = "";
});
```

4. In the JS file update the `var` variables to `const` declarations.

5. Change the callback in the `dblclick` event handler to an arrow function.

6. In the `add` function, modify the `value` parameter so it is a default parameter with the value `""`

7. Check the code runs correctly in the Browser, reminder to start the app run: `npm start` this builds the app and starts the Browser.

8. Solution for `./src/app.js`:

```JS
const dom = document.querySelector('#app');
//part 1
dom.innerHTML = `<input name='keywords'/>
                 <div id='main'></div>
                 <button id='clear'>Clear all</button>`;

document.querySelector("input").addEventListener("keyup", search);

//part 2
document.querySelector("#clear").addEventListener("click", () => {
  document.querySelector("#main").innerHTML = "";
});

//part 3 remove var variables, use arrow function
function search(event) {
  if (event.key === 'Enter') {
    const val = document.querySelector("input").value;
    // document.querySelector("#main").innerHTML = "You searched for: " + val;
    const node = add("#main", val);

    node.addEventListener("dblclick", (event) => {
      event.target.outerHTML = "";
    });

  }
	event.preventDefault();
}

//part 4 use default parameters, remove var variables
function add(id, value = "") {
  const node =  document.createElement("LI");
  const textnode = document.createTextNode(value);
  node.appendChild(textnode);
  document.querySelector(id).appendChild(node);
  return node;
}


//Bonus
function httpEcho(request) {
  fetch( "/get?keywords=" + request).then(function(response) {
    return response.json();
  }).then(function(data) {
    document.querySelector("#main").innerHTML = "You searched for:" + data.args.keywords;
  });
}
```
