#Part 1

0. In git create the branch to `ex3.2`, with `git checkout -b ex3.2`. If you didn't finish the last exercise then use `git checkout sol3.1`

1. In VSCode, edit `./src/todo.js`
- Create a class called `todo` and export it as a default, (Also remove the old default export)
- In the Class, create a constructor function that does the same as the existing `run(doc)` function
    - Inside the constructor, use `this.doc` and `this.$` to assign the properties inside the constructed object scopes
- Remove the old default export and the old `run` function
- Remove the line that exports the old `doc` and `$` variables
- Add a new alias for `this.search.bind(this)` and assign it to `this.search`, the code binds the execution of search inside the constructed object
- Note, in step 2-6 we'll refactor the code from ex3.1 and put it inside the `todo` class, we need to be careful about adding the `this.` context

Solution
```JS
export default class todo {
    constructor(doc) {
        this.doc = doc;
        this.$ = doc.querySelector.bind(doc);
        this.search = this.search.bind(this);
    }
}
```

2. In the class, create a new function called `render`, it should look like:
```JS
    render(id, content){
        this.$(id).innerHTML = content;
    }
```

3. In the class, create a new function called `append`, it should look like:
```JS
    append(id, value = "") {
        const node =  this.doc.createElement("LI");
        const textnode = this.doc.createTextNode(value);
        node.appendChild(textnode);
        this.$(id).appendChild(node);
        return node;
    }
```

4. In the class, create a new function called `on`, it should look like:
```JS
    on(id, event, fn){
        this.$(id).addEventListener(event, fn);
    }
```

5. In the class, create a new function called `initialize`, it should look like:
```JS
    initialize() {
        this.render('#app', `<input name='keywords'/>
                        <div id='main'></div>
                        <button id='clear'>Clear All</button>
                        <div id='time'></div>`);
        this.on("input", "keyup", this.search);
        this.on("#clear", "click", () => this.render("#main", ""));
    }
```

6. In the class, create a new function called `search`, it should look like:
```JS
    search(event) {
        if (event.key === 'Enter') {
          const val = this.$("input").value;
          const node = this.append("#main", val);
          node.addEventListener("dblclick", (event) => {
            event.target.outerHTML = "";
          });
        }
        event.preventDefault();
    }
```



Solution `./src/todo.js`
```JS
export default class todo {

    constructor(doc) {
        this.doc = doc;
        this.$ = doc.querySelector.bind(doc);
        this.search = this.search.bind(this);
    }

    render(id, content){
        this.$(id).innerHTML = content;
    }

    append(id, value = "") {
        const node =  this.doc.createElement("LI");
        const textnode = this.doc.createTextNode(value);
        node.appendChild(textnode);
        this.$(id).appendChild(node);
        return node;
    }

    on(id, event, fn){
        this.$(id).addEventListener(event, fn);
    }

    initialize() {
        this.render('#app', `<input name='keywords'/>
                        <div id='main'></div>
                        <button id='clear'>Clear All</button>
                        <div id='time'></div>`);
        this.on("input", "keyup", this.search);
        this.on("#clear", "click", () => this.render("#main", ""));
    }

    search(event) {
        if (event.key === 'Enter') {
          const val = this.$("input").value;
          const node = this.append("#main", val);
          node.addEventListener("dblclick", (event) => {
            event.target.outerHTML = "";
          });
        }
        event.preventDefault();
    }
}
```

7. Edit `./src/app.js`
- Change the code inside the IIFE, so it creates an instance of `todo` class
- On the next line call the instantiated objects method called `initialize()`

Solution `./src/app.js`
```JS
import todo from './todo';

(function(doc = document){
  const app = new todo(doc);
  app.initialize();
})();
```

6. Check the code runs correctly in the Browser, reminder to start the app run: `npm start` this builds the app and starts the Browser.
