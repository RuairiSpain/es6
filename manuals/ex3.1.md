#Part 1

0. In git create the branch to `ex3.1`, with `git checkout -b ex3.1`. If you didn't finish the last exercise then use `git checkout sol2.1`

1. In VSCode, create a new files called `./src/todo.js`

2. Cut and paste all the code from `./src/app.js` and `./src/todo.js`

3. In `./src/app.js`
- Import a function called run from `./todo`
- Create an IIFE that has one parameter that defaults to `document` object
- Inside the IIFE, call the function `run` with the `document` object as a parameter

Solution `./src/app.js`
```JS
import run from './todo';

(function(doc = document){
  run(doc);
})();
```

4.  In `./src/todo.js`
- export a `let` variable called `doc` and another called `$`
- export a default function called `run` that has a parameter `Doc`

Solution
```JS
export let doc, $;

export default function run(Doc){

}
```

5.  In `./src/todo.js`, inside the `run` function
- Assign parameter `Doc` into the exported variable `doc`
- Create an alias called $ for `doc.querySelector`, you'll need to use a `doc.querySelector.bind(doc)`, this sets the execution context of `$` to always use the `doc` object
- Move all of the initialization lines inside the `run()` function, the run function will be called by the IIFE when the page loads
- Rename all occurences of `document` to call the alias `doc` instead
- Rename all occurences of `document.querySelector` to call the alias `$` instead

Completed Solution for `./src/todo.js`:

```JS
export let doc, $;

export default function run(Doc){
    doc = Doc;
    $ = doc.querySelector.bind(doc);
    const dom = $('#app');

    dom.innerHTML = `<input name='keywords'/>
                    <div id='main'></div>
                    <button id='clear'>Clear all</button>
                    <div id='time'></div>`;

    $("input").addEventListener("keyup", search);

    $("#clear").addEventListener("click", () => {
    $("#main").innerHTML = "";
    });
}

function search(event) {
  if (event.key === 'Enter') {
    const val = $("input").value;
    const node = add("#main", val);

    node.addEventListener("dblclick", (event) => {
      event.target.outerHTML = "";
    });

  }
  event.preventDefault();
}

function add(id, value = "") {
  const node =  doc.createElement("LI");
  const textnode = doc.createTextNode(value);
  node.appendChild(textnode);
  $(id).appendChild(node);
  return node;
}

```

6. Check the code runs correctly in the Browser, reminder to start the app run: `npm start` this builds the app and starts the Browser.
