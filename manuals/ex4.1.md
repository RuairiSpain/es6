#Part 1

0. In git create the branch to `ex4.1`, with `git checkout -b ex4.1`. If you didn't finish the last exercise then use `git checkout sol3.2`

1. In VSCode, create a new files called `./src/geo.js`
- Create a function called `where()` and export it as default

2. Inside the `where()` function create a `new Promise`
- We are calling the native Browser API `navigator.geolocation.getCurrentPosition` and getting the longitude/latitude.  Potentially the user doesn't give us permissions to this API, or it's not implemented in the Browser.  So we need deal with `resolved` for success and `rejected` for failure.
- In the resolve callback, call `navigator.geolocation.getCurrentPosition((pos) => {resolved(pos);}`
- In the reject callback, call `rejected(err);`


Solution `./src/geo.js`:
```JS
function where() {
    let prom = new Promise((resolved, rejected) => {
        navigator.geolocation.getCurrentPosition((pos) => {
            resolved(pos);
        }, (err) => {
            rejected(err);
        });
    });
    return prom
}
export default where;
```

3. Edit `./src/index.html`
- In the HEAD, add the following snippet of HTML for a script and CSS stylesheet.  This adds a 3rd party API for mapbox, that uses openstreetmaps
```HTML
    <script src='https://api.mapbox.com/mapbox-gl-js/v0.49.0/mapbox-gl.js'></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v0.49.0/mapbox-gl.css' rel='stylesheet' />
```
- In the body, add the following DIV and script tag.  This adds a DIV container for map in WebGL and a JavaScript accessToken to use the 3rd party API
```HTML
  <div id='map' style='width: 400px; height: 300px;'></div>
  <script>
    mapboxgl.accessToken = 'pk.eyJ1IjoicnNwYWluIiwiYSI6ImNqbTd5MmNodDBnNWczdXF4cW5wNXRmMjkifQ.KhlXc4tlry11k69sQQhhIA';
  </script>
```

Complete HTML Solution `./src/index.html`:
```HTML
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title><%= htmlWebpackPlugin.options.title %></title>
    <script src='https://api.mapbox.com/mapbox-gl-js/v0.49.0/mapbox-gl.js'></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v0.49.0/mapbox-gl.css' rel='stylesheet' />
  </head>
  <body>
  <div id="app"></div>

  <div id='map' style='width: 400px; height: 300px;'></div>
  <script>
    mapboxgl.accessToken = 'pk.eyJ1IjoicnNwYWluIiwiYSI6ImNqbTd5MmNodDBnNWczdXF4cW5wNXRmMjkifQ.KhlXc4tlry11k69sQQhhIA';

  </script>
</body>
</html>
```

4. Edit `src/app.js`
- Import the `geo.js` file, with: `import where from './geo';`
- Inside the IIFE after the `app.initialize()`, add a call to `where()`
- Because `where()` returns a promise, chain a `then()` function
    - The `then` function contains two callbacks:
        - First parameter is a callback which has one parameter with the results coming from the resolved
        - Second parameter is a callback which has one parameter with the error data coming from the rejected
- In the resolved callback,
    - Console log the geolocation object
    - Create a new instance of a map with the longitude/latitude to center the map, zoom at level 13
    - See the solution for the correct syntax of the 3rd party API
- In the rejected callback, console log the error object

Solution `src/app.js`
```JS
import todo from './todo';
import where from './geo';

(function(doc = document){
  const app = new todo(doc);
  app.initialize();

  where().then((pos) => {
    console.log('Yes, succeeded:'); console.log(pos);
    new mapboxgl.Map({
      container: 'map',
      style: 'mapbox://styles/mapbox/streets-v10',
      center: [pos.coords.longitude, pos.coords.latitude], // starting position [lng, lat]
        zoom: 13 // starting zoom
      });
 }, (err) => {
    console.log('No, failed:'); console.log(err);
 });
})();
```

5. Check the code runs correctly in the Browser, reminder to start the app run: `npm start` this builds the app and starts the Browser.
