import css from './stylesheet.css'
import Hello from './hello'

(new Hello({
  target: document.querySelector('#app')
})).run()
