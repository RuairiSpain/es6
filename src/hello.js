class Hello {
  constructor(config) {
    this.target = config.target;
    this.model = {
      title: 'ES6'
    }
  }

  run() {
    this.target.innerHTML = `
      <p>
        Hello from ${this.model.title}
      </p>
    `;
  }
}

export default Hello
